#ifndef __GAME_SCENE_H__
#define __GAME_SCENE_H__

#include "cocos2d.h"
#include "Stone.h"
#include <vector>


#define _ADDCAN 0	//cpp文件中 #if _ADDCAN  ... #endif之间部分不编译


struct step
{
	char selectID;	//记录走动的棋子
	char killID;	//记录杀掉的棋子
	char ByRow;		//走动棋子的本来位置
	char ByCol;		//。。。
	//构造函数
	step(int sel_id , int kill_id , int to_row , int to_col):
		selectID(sel_id) , killID(kill_id) , ByRow(to_row) , ByCol(to_col)
	{}
};



class GameScene : public cocos2d::CCLayer
{
public:
    // 初始化
    virtual bool init();  

    // 创建这个场景，也可以不需要这个方法
    static cocos2d::CCScene* scene();
    
    // 创建一个GameScene对象指针
	CREATE_FUNC(GameScene);

/////////////////////////////////////////
//chess相关成员函数
public:

protected:
	//加载棋盘成员函数
	bool LoadChessBoard(const char*);

	//初始化32个棋子,返回值代表初始化成功的棋子数
	int InitChessStone();
	
	//选择棋子事件

	//行列坐标获取棋子id
	int RowColToStoneID(RANKS rc);
	int RowColToStoneID(int r,int c);

	//移动棋子
	bool CanMoveStone(int MoveID , int KillID , RANKS ToSite);
	bool CanMoveStone(int MoveID , int KillID , int ToRow , int ToCol);
	//获取两个行列坐标是否在同一直线，如果是，返回两个坐标之间棋子数，如果不是，返回-1
	int getP2PCount(RANKS rank1,RANKS rank2);
	//吃掉棋子并移动，下面移动函数的辅助函数
	void KillMove(int MoveID , int KillID , int ToRow , int ToCol);

	//记录走棋步骤
	void StepRecord(int MoveID , int KillID , int ToRow , int ToCol);
	//回退走棋步骤,参数为倒退的次数
	void StepRewind(unsigned int stepCount=1);

	//按类型移动棋子
	bool CanMoveChe(int MoveID , int KillID , int ToRow , int ToCol);
	bool CanMoveMa(int MoveID , int KillID , int ToRow , int ToCol);
	bool CanMoveXiang(int MoveID , int KillID , int ToRow , int ToCol);
	bool CanMoveShi(int MoveID , int KillID , int ToRow , int ToCol);
	bool CanMoveJiang(int MoveID , int KillID , int ToRow , int ToCol);
	bool CanMovePao(int MoveID , int KillID , int ToRow , int ToCol);
	bool CanMoveBing(int MoveID , int KillID , int ToRow , int ToCol);

/////////////////////////////////////////
//	重写触摸事件相关函数
	bool ccTouchBegan(CCTouch *pTouch , CCEvent *pEvent) override;
	void ccTouchEnded(CCTouch *pTouch , CCEvent *pEvent) override;


//////////////////////////////////////////
//	回调函数
	void GameScene::RewindCallBack(CCObject*);


/////////////////////////////////////////
//chess相关成员变量
public:
	Stone* _stone[32];	//32个棋子
protected:
	CCSprite * _selectsprite;	//为选择棋子叠加一个精灵，标记

	int _selectID;	//选中棋子的ID

	bool _redGo;	//是红棋走为真，黑棋走为假

	std::vector<step> StepLog;	//记录走棋步骤

private:

};

#endif // __HELLOWORLD_SCENE_H__
