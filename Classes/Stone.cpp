#include "Stone.h"

int Stone::_d = 0;	//棋盘格子的长度大小
int Stone::_x_off = 0;	//棋盘坐标(0,0)在x方向上与行列(0,0)之间的差
int Stone::_y_off = 0;

//////////////////////////////////////////////////////////////////
//	行列坐标与屏幕坐标互相转化
RANKS  Stone::ScreenToRowCol(CCPoint pt)
{
	return Stone::ScreenToRowCol(pt.x , pt.y);
}
RANKS  Stone::ScreenToRowCol(float x , float y)
{
	//偏移使得棋盘行列坐标和屏幕坐标原点重合
	x -= Stone::_x_off;		
	y -= Stone::_y_off;
	//计算行列坐标，与行列点偏离在_d/2之内,即向上取整
	int row = (int)(y / Stone::_d + 0.5);
	int col = (int)(x / Stone::_d + 0.5);
	return RANKS(row , col);
}

//	行列坐标换算屏幕坐标
CCPoint Stone::RowColToScreen(RANKS rc)
{
	return Stone::RowColToScreen(rc.row , rc.col);
}
CCPoint Stone::RowColToScreen(int r , int c)
{
	return CCPointMake(c * _d + _x_off , r * _d + _y_off);
}



////////////////////////////////////////////////////////////////
////////////////////////////////////////
//	操作棋子属性相关


//	获取棋子的屏幕坐标
CCPoint Stone::getScreenPosition()const
{	//行坐标与y轴相关，列坐标与x轴相关
	return CCPointMake(_col * _d + _x_off , _row * _d + _y_off);
}


//获取棋子存活状态
bool Stone::get_survive()const
{
	return _survive;
}
//设置棋子存活状态
void Stone::set_survive(bool survive)
{
	_survive = survive;
}
//获取棋子行列坐标
RANKS Stone::getRowCol() const
{
	return RANKS(_row , _col);
}
//设置棋子位置，通过行列坐标
bool Stone::setRowCol(const RANKS rc)
{
	if (rc.row <0 || rc.col <0 || rc.row>9 || rc.col>8) {
		return false;
	}
	_row = rc.row;
	_col = rc.col;
	setPosition(getScreenPosition());
	return true;
}
bool Stone::setRowCol(int row , int col)
{
	if (row <0 || col <0 || row>9 || col>8) {
		return false;
	}
	_row = row;
	_col = col;
	setPosition(getScreenPosition());
	return true;
}


////////////////////////////////////////////////////////////////
//初始化棋子
bool Stone::init(int id)
{
	if (id > 31 || id < 0 || !CCSprite::init()) {
		return false;
	}
	//用来创建初始化棋子的位置和类型
	struct
	{
		TYPE t;
		int row;
		int col;
	}InitStone[9] = {
		{ CHE , 0 , 0 } ,
		{ MA , 0 , 1 } ,
		{ XIANG , 0 , 2 } ,
		{ SHI , 0 , 3 } ,
		{ BING , 3 , 0 } ,
		{ BING , 3 , 2 } ,
		{ PAO , 2 , 1 } ,
		{ BING , 3 , 4 } ,
		{ JIANG , 0 , 4 }
	};
	//设置棋子颜色是红色为真
	_isRed = id < 16;	//id在[0,15]之间为红色，不然为黑色
	
	//设置棋子为存活状态
	_survive = true;

	//设置棋子行列位置和类型，棋盘大小为10行9列
	int StoneIndex = id;
	if (id < 9) {			// id [0,8]
		_type = InitStone[StoneIndex].t;
		_row = InitStone[StoneIndex].row;
		_col = InitStone[StoneIndex].col;
	}
	else if (id < 16) {		// id [9,15]
		StoneIndex = id - 9;
		_type = InitStone[StoneIndex].t;
		_row = InitStone[StoneIndex].row;	//行相同 row same
		_col = 8 - InitStone[StoneIndex].col;	//列相对竖向中轴对称
	}
	else if (id < 25) {		// id [16,24]
		StoneIndex = id - 16;
		_type = InitStone[StoneIndex].t;
		_row = 9 - InitStone[StoneIndex].row;	//行相对横向中轴对称
		_col = 8 - InitStone[StoneIndex].col;	//列相对竖向中轴对称
	}
	else {					// id [28,31]
		StoneIndex = id - 25;
		_type = InitStone[StoneIndex].t;
		_row = 9 - InitStone[StoneIndex].row;	//行相对横向中轴对称
		_col = InitStone[StoneIndex].col;	//列相同
	}
	

	//创建纹理，加载图片到Cache
	CCTexture2D * texture = NULL;
	//根据类型创建纹理图片
	const char *pStoneFileName = NULL;
	switch (_type) {
	case Stone::CHE:
		pStoneFileName = _isRed ? "rche.png" : "bche.png";
		break;
	case Stone::MA:
		pStoneFileName = _isRed ? "rma.png" : "bma.png";
		break;
	case Stone::XIANG:
		pStoneFileName = _isRed ? "rxiang.png" : "bxiang.png";
		break;
	case Stone::SHI:
		pStoneFileName = _isRed ? "rshi.png" : "bshi.png";
		break;
	case Stone::JIANG:
		pStoneFileName = _isRed ? "rjiang.png" : "bjiang.png";
		break;
	case Stone::PAO:
		pStoneFileName = _isRed ? "rpao.png" : "bpao.png";
		break;
	case Stone::BING:
		pStoneFileName = _isRed ? "rbing.png" : "bbing.png";
		break;
	default:
		break;
	}
	//直接设置纹理图片，Stone是CCSprite的派生
	texture = CCTextureCache::sharedTextureCache()->addImage(pStoneFileName);
	setTexture(texture);	//设置纹理
	setTextureRect(CCRectMake(0,0,		//设置矩形大小
		texture->getContentSize().width,
		texture->getContentSize().height));

	setPosition(getScreenPosition());	//设置位置，锚点是中心
	setScale(0.7f);	//设置缩放


	return true;
}