#include "GameScene.h"
#include "customer_define.h"

USING_NS_CC;

CCScene* GameScene::scene()
{
	// 'scene' is an autorelease object
	CCScene *scene = CCScene::create();

	// 'layer' is an autorelease object
	GameScene *layer = GameScene::create();

	// add layer as a child to scene
	scene->addChild(layer);

	// return the scene
	return scene;
}

// on "init" you need to initialize your instance
bool GameScene::init()
{
	//////////////////////////////
	// 1. 基类的初始化
	if (!CCLayer::init()) {
		return false;
	}

	/////////////////////////////////
	//2.加载棋盘
	if (!LoadChessBoard("chessboard.png")) {
		return false;
	}
	/////////////////////////////////
	//3.加载并摆放棋子
	InitChessStone();

	//4.设置红旗先走,未选中棋子
	_redGo = true;
	_selectID = -1;

	//5.清空走棋步骤记录
	StepLog.clear();
	
	//6.添加选项菜单
	CCMenu * menu = CCMenu::create();
	menu->setContentSize(CCSizeMake(WinWidth - WinHeigth , WinHeigth));
	addChild(menu);
	menu->setAnchorPoint(CCPointZero);
	menu->setPosition(ccp(WinHeigth , 0));

	CCMenuItemImage * rewind = CCMenuItemImage::create("regret.jpg" , 
													   "regret.jpg" , 
													   this , menu_selector(GameScene::RewindCallBack));
	
	menu->addChild(rewind);
	rewind->setPosition(menu->getContentSize()/2);

	//设置触摸事件，移动棋子
	setTouchEnabled(true);
	setTouchMode(kCCTouchesOneByOne);

	_selectsprite = CCSprite::create("select.png");
	_selectsprite->setVisible(false);	//设置不显示
	_selectsprite->setScale(0.8f);
	addChild(_selectsprite);


	return true;
}







///////////////////////////////////////////////////////////////////////////////////////
//	悔棋菜单项调用
void GameScene::RewindCallBack(CCObject* sender)
{
	//悔棋一步
	this->StepRewind();
	//更改为走棋方
	_redGo = !_redGo;
}












/////////////////////////////////////////////////////////////////////
//	加载棋盘
bool GameScene::LoadChessBoard(const char* pChessBoardFile)
{
	CCSprite *chessboard = CCSprite::create(pChessBoardFile);
	if (NULL != chessboard) {
		chessboard->setAnchorPoint(CCPointZero);
		chessboard->setScale(WinHeigth / chessboard->getContentSize().height);
		chessboard->setPosition(CCPointZero);
		addChild(chessboard);
		return true;
	}
	return false;
}



///////////////////////////////////////////////////////////////////////////////////////
//	初始化加载棋子
int GameScene::InitChessStone()
{
	Stone::_d = WinHeigth / 10;
	Stone::_x_off = Stone::_d;
	Stone::_y_off = Stone::_d / 2;
	int i = 0;
	for (; i < 32; ++i) {
		_stone[i] = new Stone();
		if (!_stone[i]->init(i)) {
			break;	//如果初始化不成功，直接返回，后面不再继续
		}
		addChild(_stone[i]);
	}
	return i;	//全部都成功，i=32
}



/////////////////////////////////////////////////////////////////////
//	触摸事件
bool GameScene::ccTouchBegan(CCTouch *pTouch , CCEvent *pEvent)
{
	return true;	//将触摸事件传递下去
}

//触摸结束事件，走棋
void GameScene::ccTouchEnded(CCTouch *pTouch , CCEvent *pEvent)
{
	//获取行列坐标
	RANKS rc = Stone::ScreenToRowCol(pTouch->getLocation());

	//CCLog("row = %d\tcol = %d\tID = %d" , rc.row , rc.col , _selectID);
	//CCLog("color = %s" , _redGo ? "red" : "blank");

	//是不是已经选择了要下的棋子，没有选择就先选棋子
	if (_selectID == -1) {
		//获取点击位置棋子ID
		_selectID = RowColToStoneID(rc);
		//点击的位置没有棋子
		if (_selectID == -1) {
			return;
		}
		//点击的位置棋子颜色和走棋方的颜色不一致
		if (_stone[_selectID]->_isRed != _redGo) {
			_selectID = -1;		//恢复为未选中棋子的状态
			return;
		}

		//设置SprSelect的位置
		_selectsprite->setPosition(Stone::RowColToScreen(rc));
		//设置其为可见状态
		_selectsprite->setVisible(true);

	}//=======================已经有了棋子的情况====================================
	else {
		//获取要吃的棋的ID
		int killID = RowColToStoneID(rc);

		//目的位置有棋子，并且目的位置棋子的颜色和走棋的颜色一致。那就换选择（点选一个位置是在换选择，同一个棋子，颜色）
		if (killID != -1 && (_stone[killID]->_isRed == _stone[_selectID]->_isRed)) {
			_selectID = killID;
			//设置SprSelect的位置
			_selectsprite->setPosition(Stone::RowColToScreen(rc));
			//设置其为可见状态
			//_selectsprite->setVisible(true);
			return;
		}

		//如果此处没有棋子或者不是走棋方的棋子，就移动棋子
		if (CanMoveStone(_selectID , killID , rc)) {
			//记录并走棋，先记录，再走棋
			//CCLog("-s   %d, -k  %d, --r  %d,  --c  %d,",_selectID , killID , rc.row , rc.col);
			StepRecord(_selectID , killID , _stone[_selectID]->_row , _stone[_selectID]->_col);
			KillMove(_selectID , killID , rc.row , rc.col);
			//走棋成功后，更改为对方走棋。如果目标位置不能走棋，重选目标
			_redGo = !_redGo;
			_selectID = -1;
			_selectsprite->setVisible(false);
		}


	} //end if _selectID==-1 else


	//_selectID = -1;
}

//////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////
//	移动棋子
bool GameScene::CanMoveStone(int MoveID , int KillID , RANKS ToSite)
{
	return CanMoveStone(MoveID , KillID , ToSite.row , ToSite.col);
}

bool GameScene::CanMoveStone(int MoveID , int KillID , int ToRow , int ToCol)
{
	bool SuccessMove = false;
	switch (_stone[MoveID]->_type) {
	case Stone::CHE:
		SuccessMove = CanMoveChe(MoveID , KillID , ToRow , ToCol);
		break;
	case Stone::MA:
		SuccessMove = CanMoveMa(MoveID , KillID , ToRow , ToCol);
		break;
	case Stone::XIANG:
		SuccessMove = CanMoveXiang(MoveID , KillID , ToRow , ToCol);
		break;
	case Stone::SHI:
		SuccessMove = CanMoveShi(MoveID , KillID , ToRow , ToCol);
		break;
	case Stone::JIANG:
		SuccessMove = CanMoveJiang(MoveID , KillID , ToRow , ToCol);
		break;
	case Stone::PAO:
		SuccessMove = CanMovePao(MoveID , KillID , ToRow , ToCol);
		break;
	case Stone::BING:
		SuccessMove = CanMoveBing(MoveID , KillID , ToRow , ToCol);
		break;
	default:
		break;
	}

	return SuccessMove;
}

/////////////////////////////////////////////////////////////////////////
//	行列坐标获取棋子id
int GameScene::RowColToStoneID(RANKS rc)
{
	return RowColToStoneID(rc.row , rc.col);
}

int GameScene::RowColToStoneID(int r , int c)
{
	if (r<0 || c<0 || r>9 || c>8) {
		//如果点击的位置在棋盘之外
		return -1;
	}
	//int index = r * 10 + c ;		//10行9列，计算当作一维状态的位置
	for (int i = 0; i < 32; ++i) {
		if ((_stone[i]->get_survive()) &&	//棋子存活
			(_stone[i]->_row == r) &&		//行列相同
			(_stone[i]->_col == c)) {
			return i;	//返回棋子ID
		}
	}
	return -1;	//没有找到的时候
}

//吃掉棋子并移动，下面移动函数的辅助函数
void GameScene::KillMove(int MoveID , int KillID , int ToRow , int ToCol)
{
	if (KillID != -1) {
		//吃掉对方棋子
		_stone[KillID]->_survive = false;
		_stone[KillID]->setVisible(false);
	}

	//移动
	_stone[MoveID]->setRowCol(ToRow , ToCol);
}

//记录走棋的步骤
void GameScene::StepRecord(int MoveID , int KillID , int ByRow , int ByCol)
{
	StepLog.push_back(step(MoveID , KillID , ByRow , ByCol));
}

//回退走棋的步骤
void GameScene::StepRewind(unsigned int stepCount )
{
	while ((stepCount--) && !StepLog.empty()) {
		step rst = *StepLog.rbegin();	//拷贝构造，获取最后一个
	//	CCLog("sid = %d, kid = %d, by row = %d, by col = %d" ,rst.selectID,rst.killID,rst.ByRow,rst.ByCol );
		StepLog.pop_back();	//弹出最后一个

		if (rst.killID != -1) {
			//复活吃掉对方棋子
			_stone[rst.killID]->_survive = true;
			_stone[rst.killID]->setVisible(true);
		}
		//移动回去
		_stone[rst.selectID]->setRowCol(rst.ByRow , rst.ByCol);
	}
}


//////////////////////////////////////////////////////////////////////////////
//获取两个行列坐标是否在同一直线，如果是，返回两个坐标之间棋子数，如果不是，返回-1
int GameScene::getP2PCount(RANKS rank1 , RANKS rank2)
{
	//两点不在同一行，且不在同一列
	if (rank1.row != rank2.row &&
		rank1.col != rank2.col
		) {
		return -1;
	}

	int count = 0;
	int min = 0 , max = 0;

	if (rank1.row == rank2.row) {
		//在同一行，不同的列
		min = rank1.col < rank2.col ? rank1.col : rank2.col;
		max = rank1.col > rank2.col ? rank1.col : rank2.col;

		for (int i = 0; i < 32; ++i) {
			if (_stone[i]->_row == rank1.row &&					//棋子在同一行
				_stone[i]->_survive			 &&					//且还存活着
				_stone[i]->_col>min && _stone[i]->_col < max	//且在两者之间的列
				) {
				++count;	//计数加一
			}
		}
	}
	else {	//在同一列，不同的行
		min = rank1.row < rank2.row ? rank1.row : rank2.row;
		max = rank1.row > rank2.row ? rank1.row : rank2.row;

		for (int i = 0; i < 32; ++i) {
			if (_stone[i]->_col == rank1.col &&					//棋子在同一列
				_stone[i]->_survive			 &&					//且还存活着
				_stone[i]->_row>min && _stone[i]->_row < max	//且在两者之间的行
				) {
				++count;	//计数加一
			}
		}
	}
	//CCLog("==========>>>   coutn=%d" , count);
	return count;
}

//按类型移动棋子
bool GameScene::CanMoveChe(int MoveID , int KillID , int ToRow , int ToCol)
{	//车走直线，且始终点之间不能有其他多余的棋子

	if (getP2PCount(_stone[MoveID]->getRowCol() , RANKS(ToRow , ToCol))){
		//如果不在一条直线，或者直线上还有其他的棋子
		return false;
	}

#if _ADDCAN

	//吃棋并移动
	KillMove(MoveID , KillID , ToRow , ToCol);

#endif
	return true;	//成功，返回
}

bool GameScene::CanMoveMa(int MoveID , int KillID , int ToRow , int ToCol)
{	//马走日，其有撇脚阻碍时候不能移动

	//走日字形，始终行列坐标差绝对值为(1,2)或者(2,1)
	int drow = abs(_stone[MoveID]->_row - ToRow);
	int dcol = abs(_stone[MoveID]->_col - ToCol);

	if ((drow + dcol * 10) == 21) {
		//列之间相差为2，行之间相差为1
		//撇脚的位置就是始终列坐标的平均值和起点的行坐标
		drow = _stone[MoveID]->_row;
		dcol = (_stone[MoveID]->_col + ToCol) / 2;
	}
	else if ((drow + dcol * 10) == 12) {
		//列之间相差为1，行之间相差为2
		drow = (_stone[MoveID]->_row + ToRow) / 2;
		dcol = _stone[MoveID]->_col;
	}
	else {
		return false;
	}
	//CCLog("drow = %d   dcol = %d" , drow , dcol);
	//判断蹩脚的位置是否有棋子
	if (RowColToStoneID(drow , dcol) !=-1) {
		return false;
	}
	
#if _ADDCAN

	//吃棋并移动
	KillMove(MoveID , KillID , ToRow , ToCol);
#endif

	return true;	//成功，返回
}

bool GameScene::CanMoveXiang(int MoveID , int KillID , int ToRow , int ToCol)
{	// 象/相走田，且不能过河，同样有蹩脚的位置
	//不能过河，如果红旗，行不能大于4，黑棋，行不能小于5
	if (_stone[MoveID]->_isRed && ToRow > 4) { return false; }
	if (!_stone[MoveID]->_isRed && ToRow < 5) { return false; }

	//走田字形，始终行列坐标差绝对值为(2,2)
	int drow = abs(_stone[MoveID]->_row - ToRow);
	int dcol = abs(_stone[MoveID]->_col - ToCol);
	if (drow !=2 || dcol !=2 ) {
		return false;
	}
	//撇脚的位置就是始终行列坐标的平均值(int取整是向下的)
	drow = abs(_stone[MoveID]->_row + ToRow) / 2;
	dcol = abs(_stone[MoveID]->_col + ToCol) / 2;
	//判断蹩脚的位置是否有棋子
	if (RowColToStoneID(drow , dcol) !=-1 ) {
		return false;
	}

#if _ADDCAN
	if (KillID != -1) {
		//吃掉对方棋子
		_stone[KillID]->_survive = false;
		_stone[KillID]->setVisible(false);
	}

	//移动
	_stone[MoveID]->setRowCol(ToRow , ToCol);
#endif

	return true;	//成功，返回

}

bool GameScene::CanMoveShi(int MoveID , int KillID , int ToRow , int ToCol)
{	//士只能是斜着走，一次一格，且不能走出九宫
	//不在九宫之内走
	if (ToCol <3 || ToCol >5) {return false;}
	if (_stone[MoveID]->_isRed && ToRow > 2) { return false; }
	if (!_stone[MoveID]->_isRed && ToRow < 7) { return false; }

	int drow = abs(_stone[MoveID]->_row - ToRow);
	int dcol = abs(_stone[MoveID]->_col - ToCol);
	if (drow != 1 || dcol != 1) { return false; }

#if _ADDCAN
	//吃棋并移动
	KillMove(MoveID , KillID , ToRow , ToCol);

#endif
	return true;	//成功，返回
}

bool GameScene::CanMoveJiang(int MoveID , int KillID , int ToRow , int ToCol)
{
	//将帅走，一次一格，如果将帅没有照面，就不能走出九宫
	//不在九宫所在的三列
	if (ToCol <3 || ToCol >5) { return false; }

	//红帅的ID=8，黑帅的ID=24,能够走说明两个都活着。将帅不会在同一行，只能在同一列	
	//中间没有棋子，且在同一行返回0，不在同一行，返回-1

	//CCLog("mive = %d  kill=%d" , MoveID , KillID);

	if (0 == getP2PCount(_stone[8]->getRowCol() , _stone[24]->getRowCol()) &&
		(ToRow == _stone[32 - MoveID]->_row)
		){
		//将帅照面且目标行和敌人将(帅)行在同一坐标，可以杀死对方主将(帅)
		//行列都相同，说明目标位置就是地方主将（帅）
		//吃棋并移动

#if _ADDCAN
		KillMove(MoveID , KillID , ToRow , ToCol);

#endif
		return true;	//成功，返回
	}
	
	//将帅未照面，或者点击的不是对方九宫
	if (_stone[MoveID]->_isRed && ToRow > 2) { return false; }
	if (!_stone[MoveID]->_isRed && ToRow < 7) { return false; }

	int drow = abs(_stone[MoveID]->_row - ToRow);
	int dcol = abs(_stone[MoveID]->_col - ToCol);
	//if ((drow == 1 && dcol == 0) || (drow == 0 && dcol == 1)) {
	if (drow+dcol == 1) {

#if _ADDCAN
		//吃棋并移动
		KillMove(MoveID , KillID , ToRow , ToCol);

#endif
		return true;	//成功，返回	
	}
	return false; 
}

bool GameScene::CanMovePao(int MoveID , int KillID , int ToRow , int ToCol)
{
	int count = getP2PCount(_stone[MoveID]->getRowCol() , RANKS(ToRow , ToCol));
	if( -1==count || count > 1 ||(1 == count && -1==KillID ) || (0==count && KillID !=-1))  {
		//如果不在一条直线，或者直线上还有超过1个其他棋子
		//或者中间还有一个棋子，但是目标位置没有敌方棋子
		return false;
	}

#if _ADDCAN
	//吃棋并移动
	KillMove(MoveID , KillID , ToRow , ToCol);

#endif
	return true;	//成功，返回
}

bool GameScene::CanMoveBing(int MoveID , int KillID , int ToRow , int ToCol)
{
	//只能前进，过河前不能左右走，只能直走，一次只能走一格
	int drow = ToRow - _stone[MoveID]->_row ;
	int dcol = abs(_stone[MoveID]->_col - ToCol);

	if (_redGo) {
		//是红棋子走的时候
		//不可以后退
		if (drow < 0) { return false; }

		if (_stone[MoveID]->_row <= 4) {
			//还没有过河的时候
			if (drow != 1 || dcol != 0) {
				//不是前进或者有左右移动
				return false;
			}
		}
		//过了河之后，如果不是走直走一格。两个只能其中一个为0，一个为1(向上走)
		if (drow+dcol != 1) {
			return false;
		}
	}
	else {
		//是黑棋走的时候
		//不可以后退
		if (drow > 0 ) { return false; }

		if (_stone[MoveID]->_row >= 5) {
			//还没有过河的时候
			if (drow != -1 || dcol != 0) {
				//不是前进或者有左右移动
				return false;
			}
		}
		//过了河之后，如果不是走直走一格。两个只能其中一个为0，一个为1(向上走)
		if (abs(drow) + dcol != 1) {
			return false;
		}
	}

#if _ADDCAN
	//吃棋并移动
	KillMove(MoveID , KillID , ToRow , ToCol);

#endif
	return true;	//成功，返回
}