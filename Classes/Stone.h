#ifndef __STONE_H__
#define __STONE_H__
#include "cocos2d.h"
USING_NS_CC;

struct RANKS
{	
	RANKS(int r , int c):row(r) , col(c)
	{
	}
	//行列坐标点
	int row;
	int col;

};


class Stone :
	public CCSprite
{
public:
////////////////////////////////////////
	enum TYPE 
	{
		CHE,	//车
		MA,		//马
		XIANG,	//相/象
		SHI,	//士
		JIANG,	//将/帅
		PAO,	//炮
		BING	//兵
	};
////////////////////////////////////////
//棋子相关的方法
public:
	//初始化棋子，根据ID
	bool init(int id);
	//获取棋子的屏幕上的坐标
	CCPoint getScreenPosition()const;

	//屏幕上坐标装换为棋盘行列坐标
	static RANKS ScreenToRowCol(CCPoint pt);
	static RANKS ScreenToRowCol(float x , float y);
	//行列坐标装换为屏幕上坐标
	static CCPoint RowColToScreen(RANKS rc);
	static CCPoint RowColToScreen(int r , int c);


////////////////////////////////////////
//	操作棋子属性相关
public:
	//获取棋子存活状态
	bool get_survive()const;
	//设置棋子存活状态
	void set_survive(bool survive);

	//获取棋子行列坐标
	RANKS getRowCol() const;

	//设置棋子位置，通过行列坐标
	bool setRowCol(const RANKS rc);
	bool setRowCol(int row , int col);


///////////////////////////////////////
//成员变量，棋子属性
public:
	char _ID;	//棋子id，char足够
	bool _isRed;	//是红棋子，bool占一个字节
	bool _survive;	//棋子是存活状态
	int _col;	//所在列
	int _row;	//所在行
	TYPE _type;	//是什么棋子

//////////////////////////////////////
//静态类成员，公共属性
public:
	static int _d;	//棋子的直径大小
	static int _x_off;	//棋盘坐标(0,0)在x方向上与行列(0,0)之间的差
	static int _y_off;
};

#endif //__STONE_H__